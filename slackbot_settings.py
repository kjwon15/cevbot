import os

API_TOKEN = os.getenv('SLACK_TOKEN')
ERRORS_TO = os.getenv('ERRORS_TO')

PLUGINS = [
    'plugins.cryptocurrency',
    'plugins.currency',
    'plugins.dice',
    'plugins.google',
    'plugins.help',
    'plugins.sexyarm',
]
