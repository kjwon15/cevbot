import re

from urllib.parse import parse_qs, urlparse

import requests
from fake_useragent import UserAgent
from pyquery import PyQuery

from slackbot.bot import listen_to, respond_to


SEARCH_URL = 'https://google.com/search'
UA = UserAgent()

session = requests.session()
session.headers.update({
    'User-Agent': UA.chrome,
})


def sanitize_slack_link(text):
    pattern = re.compile(r'<[^|]+\|(?P<original>[^|]+)>')
    return pattern.sub(r'\g<original>', text)


def get_links(query):

    def sanitize_link(link):
        if link.startswith('/url'):
            parsed = urlparse(link)
            params = parse_qs(parsed.query)
            link = params['q']

        return link

    response = session.get(
        SEARCH_URL,
        params={
            'q': query,
        }).text
    html = PyQuery(response)

    links = [
        sanitize_link(a.attrib['href'])
        for a in html('a.l, .r a')
    ]

    return links


@listen_to(r'^!google (.*)', re.IGNORECASE)
@respond_to(r'^!google (.*)', re.IGNORECASE)
def google_query(message, query):
    sanitized_query = sanitize_slack_link(query)
    links = get_links(sanitized_query)
    if not links:
        message.reply(':slow_parrot:')
        return
    message.reply(links[0])
