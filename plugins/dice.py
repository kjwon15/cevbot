import random

from slackbot.bot import listen_to


@listen_to(r'^!(\d+)?d(\d+)$')
def dice(message, times, space):
    space = int(space)
    if space < 2:
        message.reply('Are you kidding??')
        return
    times = int(times) if times is not None else 1

    results = [random.randint(1, space) for _ in range(times)]
    total = sum(results)

    message.reply(
        '{} → {}'.format(
            ', '.join(map(str, results)),
            total))
