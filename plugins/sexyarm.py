import math
import os
import time

from collections import namedtuple

import pyrebase
import requests
from slackbot.bot import listen_to, respond_to

from .util import static_vars


NAVER_CLIENT_ID = os.getenv('NAVER_CLIENT_ID')
NAVER_CLIENT_SECRET = os.getenv('NAVER_CLIENT_SECRET')
FIREBASE_EMAIL = os.getenv('FIREBASE_EMAIL')
FIREBASE_PASSWORD = os.getenv('FIREBASE_PASSWORD')

firebase = pyrebase.initialize_app({
    "apiKey": "AIzaSyD2ydTGjDeNjI643qw1X22sjwTz5rzuWAI",
    "authDomain": "cevbot-b389b.firebaseapp.com",
    "databaseURL": "https://cevbot-b389b.firebaseio.com",
    "projectId": "cevbot-b389b",
    "storageBucket": "cevbot-b389b.appspot.com",
    "messagingSenderId": "499004727482"

})

firebase_db = firebase.database()

naver_session = requests.Session()
naver_session.headers.update({
    'X-Naver-Client-Id': NAVER_CLIENT_ID,
    'X-Naver-Client-Secret': NAVER_CLIENT_SECRET,
})


User = namedtuple('User', (
    'userid',
    'username',
    'count',
    'level',
))


def get_firebase_user():
    return firebase.auth().sign_in_with_email_and_password(
        FIREBASE_EMAIL,
        FIREBASE_PASSWORD
    )


A = 8
R = 1.5


def get_level(count):
    return int(math.log(((R - 1) * count) / A + 1, R))


def lvl_to_exp(level):
    if R == 1:
        return level * A
    return A * (R**level - 1) / (R - 1)


def get_sexy_people():
    return list(firebase_db.child('users').get().val().keys())


@listen_to(r'^!음마 (추가|제거|목록)')
@respond_to(r'^!음마 (추가|제거|목록)')
def add_sexy_people(message, cmd):
    fb_user = get_firebase_user()
    user_id = message.body['user']
    if cmd == '추가':
        firebase_db.child('users').child(user_id).set(True, fb_user['idToken'])
        message.reply('추가 완료')
    elif cmd == '제거':
        firebase_db.child('users').child(user_id).remove(fb_user['idToken'])
        message.reply('제거 완료')
    elif cmd == '목록':
        user_ids = get_sexy_people()
        username_map = {
            userid: message.channel._client.users[userid]['name']
            for userid in user_ids
        }

        counts = firebase_db.child('counts').get().val()

        users = sorted([
            User(
                userid=userid,
                username=username_map[userid],
                count=counts.get(userid, 0),
                level=get_level(counts.get(userid, 0))
            )
            for userid in user_ids
        ], key=lambda lvl: lvl.count, reverse=True)

        message.reply('\n'.join(
            f'{user.username}: {user.level}({user.count})'
            for user in users
        ))


@listen_to(r'.*')
@static_vars(recent=[])  # [(uid, timestamp),]
def adult_check(message):
    """Watching you"""
    fb_user = get_firebase_user()

    sexy_users = get_sexy_people()
    user_id = message.body['user']
    if user_id not in sexy_users:
        print('Not matched')
        return

    try:
        resp = naver_session.get(
            'https://openapi.naver.com/v1/search/adult.json',
            params={
                'query': message.body['text'],
            }
        )

        is_adult = resp.json()['adult'] == "1"
    except:
        is_adult = False

    if is_adult:
        now = time.time()
        window_size = 5 * 60
        threshold = 3
        adult_check.recent = list(filter(
            lambda x: now - x[1] < window_size,
            adult_check.recent
        )) + [(user_id, now)]

        current_count = firebase_db.child('counts').child(user_id)\
            .get().val() or 0
        past_level = get_level(current_count)
        current_count += 1
        new_level = get_level(current_count)
        firebase_db.child('counts').child(user_id)\
            .set(current_count, fb_user['idToken'])

        message.reply('음란한 단어 사용 {}회'.format(current_count))

        recent_count = sum(1 for x in adult_check.recent if x[0] == user_id)
        if recent_count > threshold:
            message.reply('그만좀 해 이 변태야')

        if past_level != new_level:
            message.reply('축하합니다 음란마귀 {}레벨입니다.'.format(new_level))
