from decimal import Decimal
import logging
import time

import requests

from fake_useragent import UserAgent
from slackbot.bot import listen_to

UA = UserAgent()

logger = logging.getLogger(__name__)


def get_exchange_rate(currency):
    try:
        return Decimal(requests.get(
            'https://crix-api-endpoint.upbit.com/v1/crix/candles/weeks',
            params={
                'code': f'CRIX.UPBIT.KRW-{currency}',
            },
            headers={
                'User-Agent': UA.chrome,
            }
        ).json()[0]['tradePrice'])
    except Exception as e:
        logger.error(f'Failed to get KRW-{currency}: {e}')
        raise


@listen_to(r'(비트코인|스팀|스팀?달러?)\s.*얼마')
def exchange(message, currency):
    """Get current cryptocurrency exchange rate in KRW"""
    currency_map = {
        '비트코인': 'BTC',
        '스팀': 'STEEM',
        '스팀달러': 'SBD',
        '스달': 'SBD',
    }

    last_call = getattr(exchange, 'last_call', None)
    now = time.time()
    exchange.last_call = time.time()
    if last_call and now - last_call < 60:
        message.reply('그만좀 해')
        return

    try:
        currency = currency_map.get(currency)
        rate = get_exchange_rate(currency)
    except:
        message.reply('Failed to get info')
    else:
        message.reply(f'1 {currency} = {rate:,} KRW')
