from slackbot.bot import listen_to
from slackbot.manager import PluginsManager


@listen_to('^!help$')
def help(message):
    """Help *listen_to* commands"""
    commands = PluginsManager.commands

    message.reply('\n'.join(
        f'`{regex.pattern}`: {func.__doc__ or func.__name__}'
        for regex, func in commands['listen_to'].items()
    ))
