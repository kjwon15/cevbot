def static_vars(**kwargs):

    def decorator(func):
        for key, val in kwargs.items():
            setattr(func, key, val)
        return func

    return decorator
