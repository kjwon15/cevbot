import re

import requests
from slackbot.bot import listen_to


@listen_to(r'^\!cur ([0-9,]+(?:\.\d+)?) ([A-Z]{3})(?: ([A-Z]{3}))?')
def exchange(message, amount, from_, to_):
    """Get current exchange rate. `!cur <amount> <from> [to]`"""

    if to_ is None:
        # Do not use default value. the framework gives None as paremeter.
        to_ = 'KRW'
    resp = requests.get(
        'https://api.fixer.io/latest',
        params={
            'base': from_,
            'symbols': to_,
        }
    ).json()
    try:
        rate = resp['rates'][to_]
    except KeyError:
        message.reply('Failed to get curreny')
    else:
        amount = amount.replace(',', '')
        amount = int(amount) if '.' not in amount else float(amount)

        result = rate * amount

        message.reply(f'{amount:,} {from_} = {result:,.0f} {to_}')


@listen_to(r'([0-9,]+(?:\.\d+)?)\s*(?:유로)', re.IGNORECASE)
def exchange_eur(message, amount):
    """Get current EUR exchange rate"""

    resp = requests.get(
        'https://api.fixer.io/latest',
        params={
            'base': 'EUR',
            'symbols': 'KRW',
        }
    ).json()

    rate = resp['rates']['KRW']
    amount = amount.replace(',', '')
    amount = int(amount) if '.' not in amount else float(amount)

    result = rate * amount

    message.reply('{:,} EUR = {:,.0f} KRW'.format(amount, result))


@listen_to(r'(?!\!cur )([0-9,]+(?:\.\d+)?)\s*(?:달러)', re.IGNORECASE)
def exchange_usd(message, amount):
    """Get current USD exchange rate"""
    resp = requests.get(
        'https://api.fixer.io/latest',
        params={
            'base': 'USD',
            'symbols': 'KRW',
        }
    ).json()

    rate = resp['rates']['KRW']
    amount = amount.replace(',', '')
    amount = int(amount) if '.' not in amount else float(amount)

    result = rate * amount

    message.reply('{:,} USD = {:,.0f} KRW'.format(amount, result))
